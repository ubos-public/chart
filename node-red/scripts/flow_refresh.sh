#!/bin/sh
echo "node-red flow refresh api"
sleep $SLEEP_TIME_SIDECAR
token=$(curl -X POST -sSk --connect-timeout 30 --retry 50 --retry-delay 10 --data "client_id=node-red-admin&grant_type=password&scope=*&username=$USERNAME&password=$PASSWORD" $URL/auth/token | grep "^{" |  jq -r .access_token)
curl -k -X POST --connect-timeout 30 --retry 50 --retry-delay 10 -H "Authorization: Bearer $token" -H "content-type: application/json; charset=utf-8" -H "Node-RED-Deployment-Type: reload" -H "Node-RED-API-Version: v2"  --data '[
    {
        "id": "6bdcd540de315898",
        "type": "tab",
        "label": "Flow 1",
        "disabled": false,
        "info": "",
        "env": []
    },
    {
        "id": "07689987cd3dd0c0",
        "type": "inject",
        "z": "6bdcd540de315898",
        "name": "",
        "props": [
            {
                "p": "payload"
            },
            {
                "p": "topic",
                "vt": "str"
            }
        ],
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "topic": "",
        "payload": "",
        "payloadType": "date",
        "x": 200,
        "y": 120,
        "wires": [
            [
                "a68d3d3042ebdebe"
            ]
        ]
    },
    {
        "id": "a68d3d3042ebdebe",
        "type": "debug",
        "z": "6bdcd540de315898",
        "name": "debug 1",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "false",
        "statusVal": "",
        "statusType": "auto",
        "x": 360,
        "y": 100,
        "wires": []
    },
    {
        "id": "8a4f5525d7cb851d",
        "type": "complete",
        "z": "6bdcd540de315898",
        "name": "",
        "scope": [],
        "uncaught": false,
        "x": 270,
        "y": 200,
        "wires": [
            []
        ]
    }
]' $URL/flows
